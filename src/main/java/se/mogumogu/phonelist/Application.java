package se.mogumogu.phonelist;

public final class Application {

    public static void main(String[] args) {

        try {
            final PhoneListHandler phoneListHandler = new PhoneListHandler();
            phoneListHandler.testPhoneLists();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
