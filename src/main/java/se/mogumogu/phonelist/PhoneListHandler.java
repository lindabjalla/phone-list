package se.mogumogu.phonelist;

import java.util.*;

final class PhoneListHandler {

    private int getNumberOfTestCases(final Kattio kattio) {

        final int numberOfTestCases = kattio.getInt();

        if (numberOfTestCases < 1 || numberOfTestCases > 40) {
            throw new InputMismatchException("The first line must be a number between 1 and 40");
        }

        return numberOfTestCases;
    }

    void testPhoneLists() throws InputMismatchException {

        final Kattio kattio = new Kattio(System.in);
        final int numberOfTestCases = getNumberOfTestCases(kattio);

        for (int i = 0; i < numberOfTestCases; i++) {
            int numberOfPhoneNumbers = getNumberOfPhoneNumbers(kattio);
            final Set<String> phoneNumbers = new HashSet<>();
            getPhoneNumbers(kattio, numberOfPhoneNumbers, phoneNumbers);
            getAnswer(phoneNumbers);
        }
    }

    private int getNumberOfPhoneNumbers(final Kattio kattio) {

        int numberOfPhoneNumbers = kattio.getInt();

        if (numberOfPhoneNumbers < 1 || numberOfPhoneNumbers > 10000) {
            throw new InputMismatchException("Number of phone numbers must be between 1 and 10000");
        }

        return numberOfPhoneNumbers;
    }

    private void getPhoneNumbers(final Kattio kattio, final int numberOfPhoneNumbers, final Set<String> phoneNumbers) {

        for (int j = 0; j < numberOfPhoneNumbers; j++) {

            final String phoneNumberString = kattio.getWord();

            if (phoneNumberString.length() > 10) {
                throw new InputMismatchException("A phone number longer than 10 digits are not allowed");
            }

            phoneNumbers.add(phoneNumberString);
        }
    }

    private void getAnswer(final Set<String> testCase) {

        final ArrayList<String> list = new ArrayList<>(testCase);
        Collections.sort(list);

        final boolean consistent = checkIfConsistent(list);

        if (consistent) {
            System.out.println("YES");

        } else {
            System.out.println("NO");
        }
    }

    private boolean checkIfConsistent(final List<String> phoneNumbers) {

        for (int i = 0; i < phoneNumbers.size() - 1; i++) {

            final String prefix = phoneNumbers.get(i);
            final String phoneNumber = phoneNumbers.get(i + 1);

            if (phoneNumber.startsWith(prefix)) {
                return false;
            }
        }

        return true;
    }
}
